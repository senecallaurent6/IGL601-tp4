package tp4;

import java.util.List;
import javax.persistence.TypedQuery;

public class Parties
{
    private Connexion cx;
    private TypedQuery<Partie> trouver;

    public Parties(Connexion cx)
    {
        this.cx = cx;
        trouver = cx.getConnection().createQuery("select tuple from Partie tuple where tuple.id=:id", Partie.class);
    }

    public Partie trouver(int id)
    {
        trouver.setParameter("id", id);
        List<Partie> tuples = trouver.getResultList();
        return tuples.isEmpty() ? null : tuples.get(0);
    }

    public boolean existe(int id)
    {
        return trouver(id) != null;
    }

    public void ajouter(Partie tuple)
    {
        cx.getConnection().persist(tuple);
    }
}
