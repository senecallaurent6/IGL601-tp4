package tp4;

import java.util.Calendar;

public class GestionProces
{
    private Connexion cx;
    private Process tableProces;
    private Parties tablePartie;
    private Juges tableJuge;
    private Seances tableSeance;

    public GestionProces(Connexion cx, Process tableProces, Parties tablePartie, Juges tableJuge, Seances tableSeance)
    {
        this.cx = cx;
        this.tableProces = tableProces;
        this.tablePartie = tablePartie;
        this.tableJuge = tableJuge;
        this.tableSeance = tableSeance;
    }

    public void creer(int id, int idJuge, java.sql.Date dateInitiale, int devantJuryInt, int idPartieDefenderess,
            int idPartiePoursuivante) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            if (devantJuryInt != 0 && devantJuryInt != 1)
            {
                throw new IFT287Exception("Le champ <devantJury> doit être 0 ou 1");
            }
            boolean devantJury = devantJuryInt > 0;

            if (idPartieDefenderess == idPartiePoursuivante)
            {
                throw new IFT287Exception("Un partie ne peut pas se poursuivre soi-même.");
            }

            if (tableProces.existe(id))
            {
                throw new IFT287Exception("Un proces porte déjà cet id.");
            }

            Juge juge = tableJuge.trouver(idJuge);
            if (juge == null)
            {
                throw new IFT287Exception("Aucun juge porte l'id juge specifié.");
            }

            if (!juge.estActif())
            {
                throw new IFT287Exception("Le juge specifié n'est pas actif.");
            }

            Partie partieDefenderess = tablePartie.trouver(idPartieDefenderess);
            Partie partiePoursuivante = tablePartie.trouver(idPartiePoursuivante);

            if (partieDefenderess == null)
            {
                throw new IFT287Exception("Aucun partie porte l'id de la defenderess.");
            }
            if (partiePoursuivante == null)
            {
                throw new IFT287Exception("Aucun partie porte l'id de la partie poursuivante.");
            }

            if (partieDefenderess.getAvocat().equals(partiePoursuivante.getAvocat()))
            {
                throw new IFT287Exception("Les deux parties ont le même avocat.");
            }

            tableProces.ajouter(
                    new Proces(id, juge, partieDefenderess, partiePoursuivante, dateInitiale, null, devantJury));

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public void terminer(int id, int decisionInt) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            if (decisionInt != 0 && decisionInt != 1)
            {
                throw new IFT287Exception("La décision doit être 0 ou 1");
            }
            boolean decision = decisionInt > 0;

            Proces proces = tableProces.trouver(id);
            if (proces == null)
            {
                throw new IFT287Exception("Le procès n'existe pas.");
            }

            if (Calendar.getInstance().getTime().getTime() < proces.getDateInitiale().getTime())
            {
                throw new IFT287Exception("Impossible de terminer un procès qui n'a pas commencé.");
            }

            if (proces.estTermine())
            {
                throw new IFT287Exception("Impossible de changer la décision d'un procès terminé.");
            }

            proces.terminer(decision);

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public void ajouterSeance(int idSeance, int idProces, java.sql.Date date) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            if (tableSeance.existe(idSeance))
            {
                throw new IFT287Exception("Une seance porte déjà cet id.");
            }

            Proces proces = tableProces.trouver(idProces);
            if (proces == null)
            {
                throw new IFT287Exception("Le proces specifié n'existe pas.");
            }

            if (proces.estTermine())
            {
                throw new IFT287Exception("Le proces specifié est terminé.");
            }

            if (date.getTime() < proces.getDateInitiale().getTime())
            {
                throw new IFT287Exception("Le procès n'aura pas commencé à la date de la seance.");
            }

            proces.addSeance(new Seance(idSeance, proces, date));

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public void supprimerSeance(int id) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            Seance seance = tableSeance.trouver(id);
            if (seance == null)
            {
                throw new IFT287Exception("La seance n'existe pas.");
            }

            if (seance.getDateSeance().getTime() <= Calendar.getInstance().getTime().getTime())
            {
                throw new IFT287Exception("Impossible de supprimer une séance passée ou en cours.");
            }

            Proces proces = seance.getProces();
            if (proces == null)
            {
                throw new IFT287Exception("Le procès de la seance n'existe pas (ceci ne devrait jamais arriver).");
            }

            proces.supprimerSeance(seance);

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public Proces trouver(int id) throws IFT287Exception
    {
        Proces proces = tableProces.trouver(id);
        if (proces == null)
        {
            throw new IFT287Exception("Le proces n'existe pas.");
        }
        return proces;
    }
}
