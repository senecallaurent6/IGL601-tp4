package tp4;

import java.util.List;

import javax.persistence.TypedQuery;

public class Jurys
{
    private Connexion cx;
    private TypedQuery<Jury> trouver;
    private TypedQuery<Jury> trouverDisponibles;

    public Jurys(Connexion cx)
    {
        this.cx = cx;
        trouver = cx.getConnection().createQuery("select tuple from Jury tuple where tuple.nas=:id", Jury.class);
        trouverDisponibles = cx.getConnection().createQuery("select j from Jury j where j.estDisponible()", Jury.class);
    }

    public Jury trouver(int id)
    {
        trouver.setParameter("id", id);
        List<Jury> tuples = trouver.getResultList();
        return tuples.isEmpty() ? null : tuples.get(0);
    }

    public boolean existe(int id)
    {
        return trouver(id) != null;
    }

    public void ajouter(Jury tuple)
    {
        cx.getConnection().persist(tuple);
    }

    public List<Jury> trouverDisponibles()
    {
        return trouverDisponibles.getResultList();
    }
}
