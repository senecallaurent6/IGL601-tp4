package tp4;

import java.util.List;

public class GestionJuge
{
    private Connexion cx;
    private Juges tableJuge;

    public GestionJuge(Connexion cx, Juges tableJuge)
    {
        this.cx = cx;
        this.tableJuge = tableJuge;
    }

    public void ajouter(int id, String prenom, String nom, int age) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            if (tableJuge.existe(id))
            {
                throw new IFT287Exception("Un juge porte déjà cet id.");
            }

            tableJuge.ajouter(new Juge(id, prenom, nom, age, true));

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public void retirer(int id) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            Juge juge = tableJuge.trouver(id);
            if (juge == null)
            {
                throw new IFT287Exception("Impossible de retirer un juge qui n'existe pas.");
            }

            if (!juge.estActif())
            {
                throw new IFT287Exception("Le juge est déjà retiré.");
            }

            juge.retirer();

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public List<Juge> trouverActifs()
    {
        return tableJuge.trouverActifs();
    }

    public Juge trouver(int id) throws IFT287Exception
    {
        Juge juge = tableJuge.trouver(id);
        if (juge == null)
        {
            throw new IFT287Exception("Le juge n'existe pas.");
        }
        return juge;
    }
}
