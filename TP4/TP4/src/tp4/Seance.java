package tp4;

import java.sql.Date;

import javax.persistence.*;

@Entity
public class Seance
{
    @Id
    @GeneratedValue
    private long idBD;
    private int id;
    private Date dateSeance;

    @ManyToOne()
    private Proces proces;

    public Seance(int id, Proces proces, Date dateSeance)
    {
        setId(id);
        setProces(proces);
        setDateSeance(dateSeance);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setProces(Proces proces)
    {
        this.proces = proces;
    }

    public Proces getProces()
    {
        return proces;
    }

    public Date getDateSeance()
    {
        return dateSeance;
    }

    public void setDateSeance(Date dateSeance)
    {
        this.dateSeance = dateSeance;
    }
}
