package tp4;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Juge
{
    @Id
    @GeneratedValue
    private long idBD;
    private int id;
    private String prenom;
    private String nom;
    private int age;
    private boolean estActif;

    public Juge(int id, String prenom, String nom, int age, boolean estActif)
    {
        setId(id);
        setPrenom(prenom);
        setNom(nom);
        setAge(age);
        setEstActif(estActif);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public boolean estActif()
    {
        return estActif;
    }

    public void setEstActif(boolean estActif)
    {
        this.estActif = estActif;
    }

    public void retirer()
    {
        this.estActif = false;
    }
}
