package tp4;

public class GestionAvocat
{
    private Avocats tableAvocat;
    private Connexion cx;

    public GestionAvocat(Connexion cx, Avocats avocat)
    {
        this.cx = cx;
        this.tableAvocat = avocat;
    }

    public void ajouter(int id, String prenom, String nom, int type) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            if (tableAvocat.existe(id))
            {
                throw new IFT287Exception("Un avocat porte déjà cet id.");
            }

            Avocat a = new Avocat(id, prenom, nom, type);

            tableAvocat.ajouter(a);

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw e;
        }
    }

    public Avocat trouver(int id) throws IFT287Exception
    {
        Avocat avocat = tableAvocat.trouver(id);
        if (avocat == null)
        {
            throw new IFT287Exception("L'avocat n'existe pas.");
        }
        return avocat;
    }
}
