package tp4;

import java.sql.Date;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Proces
{
    @Id
    @GeneratedValue
    private long idBD;
    private int id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Juge juge;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Partie partieDefenderess;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Partie partiePoursuivante;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    List<Jury> jurys;

    @OneToMany(cascade = CascadeType.PERSIST)
    List<Seance> seances;

    private Date dateInitiale;
    private Boolean decision;
    private boolean devantJury;

    Proces(int id, Juge juge, Partie partieDefenderess, Partie partiePoursuivante, Date dateInitiale, Boolean decision,
            boolean devantJury)
    {
        setId(id);
        setJuge(juge);
        setPartieDefenderess(partieDefenderess);
        setPartiePoursuivante(partiePoursuivante);
        setDateInitiale(dateInitiale);
        setDecision(decision);
        setDevantJury(devantJury);

        jurys = new LinkedList<Jury>();
        seances = new LinkedList<Seance>();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Juge getJuge()
    {
        return juge;
    }

    public void setJuge(Juge juge)
    {
        this.juge = juge;
    }

    public Partie getPartieDefenderess()
    {
        return partieDefenderess;
    }

    public void setPartieDefenderess(Partie partieDefenderess)
    {
        this.partieDefenderess = partieDefenderess;
    }

    public Partie getPartiePoursuivante()
    {
        return partiePoursuivante;
    }

    public void setPartiePoursuivante(Partie partiePoursuivante)
    {
        this.partiePoursuivante = partiePoursuivante;
    }

    public List<Jury> getJurys()
    {
        return jurys;
    }

    public void ajouterJury(Jury jury) throws IFT287Exception
    {
        if (!devantJury)
        {
            throw new IFT287Exception("Le proces ne se tient pas devant Jury");
        }
        jurys.add(jury);
    }

    public List<Seance> getSeances()
    {
        return seances;
    }

    public void addSeance(Seance seance)
    {
        seances.add(seance);
    }

    public void supprimerSeance(Seance seance) throws IFT287Exception
    {
        if (estTermine())
        {
            throw new IFT287Exception("Impossible de supprimer une seance d'un procès terminé.");
        }

        seances.remove(seance);
    }

    public Date getDateInitiale()
    {
        return dateInitiale;
    }

    public void setDateInitiale(Date dateInitiale)
    {
        this.dateInitiale = dateInitiale;
    }

    public Boolean getDecision()
    {
        return decision;
    }

    public boolean estTermine()
    {
        return decision != null;
    }

    public void terminer(Boolean decision) throws IFT287Exception
    {
        if (estTermine())
        {
            throw new IFT287Exception("Le proces est déjà terminé");
        }
        setDecision(decision);

        List<Seance> oldSeances = seances;
        seances = new LinkedList<Seance>();

        for (Seance s : oldSeances)
        {
            if (s.getDateSeance().getTime() <= Calendar.getInstance().getTime().getTime())
            {
                addSeance(s);
            }
        }
    }

    private void setDecision(Boolean decision)
    {
        this.decision = decision;
    }

    public boolean isDevantJury()
    {
        return devantJury;
    }

    public void setDevantJury(boolean devantJury)
    {
        this.devantJury = devantJury;
    }
}
