package tp4;

import java.util.List;
import javax.persistence.TypedQuery;

public class Juges
{
    private Connexion cx;
    private TypedQuery<Juge> trouver;
    private TypedQuery<Juge> trouverActifs;

    public Juges(Connexion cx)
    {
        this.cx = cx;
        trouver = cx.getConnection().createQuery("select tuple from Juge tuple where tuple.id=:id", Juge.class);
        trouverActifs = cx.getConnection().createQuery("select j from Juge j where j.estActif()", Juge.class);
    }

    public Juge trouver(int id)
    {
        trouver.setParameter("id", id);
        List<Juge> tuples = trouver.getResultList();
        return tuples.isEmpty() ? null : tuples.get(0);
    }

    public boolean existe(int id)
    {
        return trouver(id) != null;
    }

    public void ajouter(Juge tuple)
    {
        cx.getConnection().persist(tuple);
    }

    public List<Juge> trouverActifs()
    {
        return trouverActifs.getResultList();
    }
}
