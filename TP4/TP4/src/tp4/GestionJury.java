package tp4;

import java.util.List;

public class GestionJury
{
    private Connexion cx;
    private Jurys tableJury;
    private Process tableProces;

    public GestionJury(Connexion cx, Jurys tableJury, Process tableProces)
    {
        this.cx = cx;
        this.tableJury = tableJury;
        this.tableProces = tableProces;
    }

    public void inscrire(int nas, String prenom, String nom, String sexe, int age) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            if (tableJury.existe(nas))
            {
                throw new IFT287Exception("Un jury porte déjà ce NAS.");
            }

            tableJury.ajouter(new Jury(nas, prenom, nom, sexe, age));

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public void assigner(int nas, int idProces) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            Jury jury = tableJury.trouver(nas);
            if (jury == null)
            {
                throw new IFT287Exception("Le jury n'existe pas.");
            }

            if (!jury.estDisponible())
            {
                throw new IFT287Exception("Le jury n'est pas disponible.");
            }

            Proces proces = tableProces.trouver(idProces);
            if (proces == null)
            {
                throw new IFT287Exception("Le proces n'existe pas.");
            }

            if (proces.estTermine())
            {
                throw new IFT287Exception("Le proces est terminé.");
            }

            if (!proces.isDevantJury())
            {
                throw new IFT287Exception("Ce proces ne se tient pas devant jury.");
            }

            proces.ajouterJury(jury);
            jury.addProces(proces);

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public List<Jury> trouverDisponibles()
    {
        return tableJury.trouverDisponibles();
    }
}
