package tp4;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Avocat
{
    @Id
    @GeneratedValue
    private long idBD;
    private int id;
    private String nom;
    private String prenom;
    private int type;

    public Avocat(int id, String prenom, String nom, int type) throws IFT287Exception
    {
        setId(id);
        setNom(nom);
        setPrenom(prenom);
        setType(type);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type) throws IFT287Exception
    {
        if (type != 0 && type != 1)
        {
            throw new IFT287Exception("Le champ <type> doit être 0 ou 1");
        }
        this.type = type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + (int) (idBD ^ (idBD >>> 32));
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
        result = prime * result + type;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Avocat other = (Avocat) obj;
        if (id != other.id)
            return false;
        if (idBD != other.idBD)
            return false;
        if (nom == null)
        {
            if (other.nom != null)
                return false;
        }
        else if (!nom.equals(other.nom))
            return false;
        if (prenom == null)
        {
            if (other.prenom != null)
                return false;
        }
        else if (!prenom.equals(other.prenom))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

}
