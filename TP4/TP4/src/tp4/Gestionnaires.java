package tp4;

import java.sql.SQLException;

public class Gestionnaires
{
    private Connexion cx;

    private Avocats tableAvocat;
    private Juges tableJuge;
    private Jurys tableJury;
    private Parties tablePartie;
    private Process tableProces;
    private Seances tableSeance;

    private GestionAvocat gestionAvocat;
    private GestionJuge gestionJuge;
    private GestionJury gestionJury;
    private GestionPartie gestionPartie;
    private GestionProces gestionProces;

    public Gestionnaires(String serveur, String bd, String user, String password) throws SQLException, IFT287Exception
    {
        cx = new Connexion(serveur, bd, user, password);

        tableAvocat = new Avocats(cx);
        tableJuge = new Juges(cx);
        tableJury = new Jurys(cx);
        tablePartie = new Parties(cx);
        tableProces = new Process(cx);
        tableSeance = new Seances(cx);

        gestionAvocat = new GestionAvocat(cx, tableAvocat);
        gestionJuge = new GestionJuge(cx, tableJuge);
        gestionJury = new GestionJury(cx, tableJury, tableProces);
        gestionPartie = new GestionPartie(cx, tablePartie, tableAvocat);
        gestionProces = new GestionProces(cx, tableProces, tablePartie, tableJuge, tableSeance);
    }

    public void fermer() throws SQLException
    {
        if (cx != null)
        {
            cx.fermer();
        }
    }

    public Connexion getConnexion()
    {
        return cx;
    }

    public GestionAvocat getGestionAvocat()
    {
        return gestionAvocat;
    }

    public GestionJuge getGestionJuge()
    {
        return gestionJuge;
    }

    public GestionJury getGestionJury()
    {
        return gestionJury;
    }

    public GestionPartie getGestionPartie()
    {
        return gestionPartie;
    }

    public GestionProces getGestionProces()
    {
        return gestionProces;
    }
}
