package tp4;

import javax.persistence.*;

@Entity
public class Partie
{
    @Id
    @GeneratedValue
    private long idBD;
    private int id;

    @OneToOne(cascade = CascadeType.PERSIST)
    private Avocat avocat;

    private String prenom;
    private String nom;

    public Partie(int id, String prenom, String nom, Avocat avocat)
    {
        setId(id);
        setAvocat(avocat);
        setPrenom(prenom);
        setNom(nom);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Avocat getAvocat()
    {
        return avocat;
    }

    public void setAvocat(Avocat avocat)
    {
        this.avocat = avocat;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }
}
