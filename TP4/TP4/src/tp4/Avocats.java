package tp4;

import java.util.List;

import javax.persistence.TypedQuery;

public class Avocats
{
    private Connexion cx;
    private TypedQuery<Avocat> trouver;

    public Avocats(Connexion cx)
    {
        this.cx = cx;
        trouver = cx.getConnection().createQuery("select tuple from Avocat tuple where tuple.id=:id", Avocat.class);
    }

    public Avocat trouver(int id) throws IFT287Exception
    {
        trouver.setParameter("id", id);
        List<Avocat> tuples = trouver.getResultList();
        return tuples.isEmpty() ? null : tuples.get(0);
    }

    public boolean existe(int id) throws IFT287Exception
    {
        return trouver(id) != null;
    }

    public void ajouter(Avocat tuple)
    {
        cx.getConnection().persist(tuple);
    }
}
