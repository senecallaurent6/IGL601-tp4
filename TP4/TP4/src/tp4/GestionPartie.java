package tp4;

public class GestionPartie
{
    private Connexion cx;
    private Parties tablePartie;
    private Avocats tableAvocat;

    public GestionPartie(Connexion cx, Parties tablePartie, Avocats tableAvocat)
    {
        this.cx = cx;
        this.tablePartie = tablePartie;
        this.tableAvocat = tableAvocat;
    }

    public void ajouter(int id, String prenom, String nom, int idAvocat) throws IFT287Exception
    {
        try
        {
            cx.demarreTransaction();

            if (tablePartie.existe(id))
            {
                throw new IFT287Exception("Un partie porte déjà cet id.");
            }

            Avocat avocat = tableAvocat.trouver(idAvocat);
            if (avocat == null)
            {
                throw new IFT287Exception("L'avocat n'existe pas.");
            }

            tablePartie.ajouter(new Partie(id, prenom, nom, avocat));

            cx.commit();
        }
        catch (Exception e)
        {
            cx.rollback();
            throw (e);
        }
    }

    public Partie trouverPartie(int id) throws IFT287Exception
    {
        Partie partie = tablePartie.trouver(id);
        if (partie == null)
        {
            throw new IFT287Exception("Le partie n'existe pas.");
        }
        return partie;
    }
}
