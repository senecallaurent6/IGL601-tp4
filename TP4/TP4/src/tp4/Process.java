package tp4;

import java.util.List;
import javax.persistence.TypedQuery;

public class Process
{
    private Connexion cx;
    private TypedQuery<Proces> trouver;

    public Process(Connexion cx)
    {
        this.cx = cx;
        trouver = cx.getConnection().createQuery("select tuple from Proces tuple where tuple.id=:id", Proces.class);
    }

    public Proces trouver(int id)
    {
        trouver.setParameter("id", id);
        List<Proces> tuples = trouver.getResultList();
        return tuples.isEmpty() ? null : tuples.get(0);
    }

    public boolean existe(int id)
    {
        return trouver(id) != null;
    }

    public void ajouter(Proces tuple)
    {
        cx.getConnection().persist(tuple);
    }
}
