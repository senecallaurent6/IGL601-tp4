// Travail fait par :
// Laurence Fournier - 10 117 465
// Ruslan Otroc - 15 203 405

package tp4;

import java.io.*;
import java.sql.*;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Fichier de base pour le TP2 du cours IFT287
 *
 * <pre>
 * 
 * Vincent Ducharme
 * Universite de Sherbrooke
 * Version 1.0 - 7 juillet 2016
 * IFT287 - Exploitation de BD relationnelles et OO
 * 
 * Ce programme permet d'appeler des transactions d'un systeme
 * de gestion utilisant une base de donnees.
 *
 * Paramètres du programme
 * 0- site du serveur SQL ("local" ou "dinf")
 * 1- nom de la BD
 * 2- user id pour etablir une connexion avec le serveur SQL
 * 3- mot de passe pour le user id
 * 4- fichier de transaction [optionnel]
 *           si non spécifié, les transactions sont lues au
 *           clavier (System.in)
 *
 * Pré-condition
 *   - La base de donnees doit exister
 *
 * Post-condition
 *   - Le programme effectue les mises à jour associees à chaque
 *     transaction
 * </pre>
 */
public class Devoir4
{
    private static Gestionnaires gests;

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception
    {
        if (args.length < 4)
        {
            System.out.println("Usage: java tp2.Devoir2 <serveur> <bd> <user> <password> [<fichier-transactions>]");
            return;
        }

        try
        {
            gests = new Gestionnaires(args[0], args[1], args[2], args[3]);

            BufferedReader reader = ouvrirFichier(args);
            String transaction = lireTransaction(reader);
            while (!finTransaction(transaction))
            {
                executerTransaction(transaction);
                transaction = lireTransaction(reader);
                System.out.println("");
            }
        }
        finally
        {
            if (gests != null)
            {
                gests.fermer();
            }
        }
    }

    /**
     * Decodage et traitement d'une transaction
     */
    static void executerTransaction(String transaction)
    {
        try
        {
            System.out.print(transaction);
            // Decoupage de la transaction en mots
            StringTokenizer tokenizer = new StringTokenizer(transaction, " ");
            if (tokenizer.hasMoreTokens())
            {
                String command = tokenizer.nextToken();
                // Vous devez remplacer la chaine "commande1" et "commande2" par
                // les commandes de votre programme. Vous pouvez ajouter autant
                // de else if que necessaire. Vous n'avez pas a traiter la
                // commande "quitter".
                if (command.equals("ajouterJuge"))
                {
                    gests.getGestionJuge().ajouter(readInt(tokenizer), readString(tokenizer), readString(tokenizer),
                            readInt(tokenizer));
                }
                else if (command.equals("retirerJuge"))
                {
                    gests.getGestionJuge().retirer(readInt(tokenizer));
                }
                else if (command.equals("ajouterAvocat"))
                {
                    gests.getGestionAvocat().ajouter(readInt(tokenizer), readString(tokenizer), readString(tokenizer),
                            readInt(tokenizer));
                }
                else if (command.equals("ajouterPartie"))
                {
                    gests.getGestionPartie().ajouter(readInt(tokenizer), readString(tokenizer), readString(tokenizer),
                            readInt(tokenizer));
                }
                else if (command.equals("creerProces"))
                {
                    gests.getGestionProces().creer(readInt(tokenizer), readInt(tokenizer), readDate(tokenizer),
                            readInt(tokenizer), readInt(tokenizer), readInt(tokenizer));
                }
                else if (command.equals("inscrireJury"))
                {
                    gests.getGestionJury().inscrire(readInt(tokenizer), readString(tokenizer), readString(tokenizer),
                            readString(tokenizer), readInt(tokenizer));
                }
                else if (command.equals("assignerJury"))
                {
                    gests.getGestionJury().assigner(readInt(tokenizer), readInt(tokenizer));
                }
                else if (command.equals("ajouterSeance"))
                {
                    gests.getGestionProces().ajouterSeance(readInt(tokenizer), readInt(tokenizer), readDate(tokenizer));
                }
                else if (command.equals("supprimerSeance"))
                {
                    gests.getGestionProces().supprimerSeance(readInt(tokenizer));
                }
                else if (command.equals("terminerProces"))
                {
                    gests.getGestionProces().terminer(readInt(tokenizer), readInt(tokenizer));
                }
                else if (command.equals("afficherJuges"))
                {
                    afficherJuges();
                }
                else if (command.equals("afficherProces"))
                {
                    afficherProces(readInt(tokenizer));
                }
                else if (command.equals("afficherJurys"))
                {
                    afficherJurys();
                }
                else if (command.equals("quitter"))
                {
                    return;
                }
                else
                {
                    System.out.println(" : Transaction non reconnue");
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("");
            System.out.println(e.toString());
            // e.printStackTrace();
        }
    }

    // Cette methode est a redefinir pour chacune de vos transactions.
    // Vous devez donner un nom significatif et utiliser les bons types de
    // parametres.
    // La methode donnee dans ce fichier n'est qu'un exemple.

    public static void afficherJuges()
    {
        List<Juge> juges = gests.getGestionJuge().trouverActifs();

        System.out.println("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        for (Juge j : juges)
        {
            System.out.println(j.getId() + ", " + j.getPrenom() + " " + j.getNom() + ", " + j.getAge() + " ans");
        }
        System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

    public static void afficherProces(int id) throws IFT287Exception
    {
        Proces proces = gests.getGestionProces().trouver(id);

        // Dates
        System.out.println("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

        // Terminé?
        System.out.print("Proces commencé le " + proces.getDateInitiale());
        if (proces.estTermine())
        {
            System.out.println(" terminé.");
            System.out.println(
                    proces.getDecision() ? "La partie defenderess a gagnée." : "La partie poursuivante a gagnée.");
        }
        else
        {
            System.out.println(" non terminé.");
        }

        // Juge
        Juge juge = proces.getJuge();
        System.out.println("\nJuge: " + juge.getPrenom() + " " + juge.getNom());

        // Partie et Avocats
        System.out.println("\nPartie poursuivante : ");
        afficherPartieEtAvocat(proces.getPartiePoursuivante());
        System.out.println("\nPartie defenderess : ");
        afficherPartieEtAvocat(proces.getPartieDefenderess());

        // Jurys
        if (proces.isDevantJury())
        {
            System.out.println("\nJurys :");
            List<Jury> jurys = proces.getJurys();
            for (Jury j : jurys)
            {
                System.out.println(j.getPrenom() + " " + j.getNom() + ", " + j.getSexe() + " " + j.getAge() + " ans");
            }
        }
        else
        {
            System.out.println("\nCe procès ne se tient pas devant jury.");
        }

        // Seances
        List<Seance> seances = proces.getSeances();
        System.out.println("\nDates des seances passées et à venir :");
        for (Seance s : seances)
        {
            System.out.println(s.getDateSeance());
        }
        System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

    private static void afficherPartieEtAvocat(Partie partie)
    {
        System.out.println(partie.getPrenom() + " " + partie.getNom());

        Avocat avocat = partie.getAvocat();

        System.out.println("Représenté par " + avocat.getPrenom() + " " + avocat.getNom());
        if (avocat.getType() == 0)
        {
            System.out.println("(avocat privé)");
        }
        else
        {
            System.out.println("(avocat du directeur des poursuites criminelles et pénales)");
        }
    }

    public static void afficherJurys() throws SQLException
    {
        List<Jury> jurys = gests.getGestionJury().trouverDisponibles();
        System.out.println("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        for (Jury j : jurys)
        {
            System.out.println(j.getPrenom() + " " + j.getNom() + ", " + j.getNas() + " " + j.getSexe() + " "
                    + j.getAge() + " ans");
        }
        System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

    // ****************************************************************
    // * Les methodes suivantes n'ont pas besoin d'etre modifiees *
    // ****************************************************************

    /**
     * Ouvre le fichier de transaction, ou lit à partir de System.in
     */
    public static BufferedReader ouvrirFichier(String[] args) throws FileNotFoundException
    {
        if (args.length < 5)
            // Lecture au clavier
            return new BufferedReader(new InputStreamReader(System.in));
        else
            // Lecture dans le fichier passe en parametre
            return new BufferedReader(new InputStreamReader(new FileInputStream(args[4])));
    }

    /**
     * Lecture d'une transaction
     */
    static String lireTransaction(BufferedReader reader) throws IOException
    {
        return reader.readLine();
    }

    /**
     * Verifie si la fin du traitement des transactions est atteinte.
     */
    static boolean finTransaction(String transaction)
    {
        // fin de fichier atteinte
        return (transaction == null || transaction.equals("quitter"));
    }

    /** Lecture d'une chaine de caracteres de la transaction entree a l'ecran */
    static String readString(StringTokenizer tokenizer) throws Exception
    {
        if (tokenizer.hasMoreElements())
            return tokenizer.nextToken();
        else
            throw new Exception("Autre parametre attendu");
    }

    /**
     * Lecture d'un int java de la transaction entree a l'ecran
     */
    static int readInt(StringTokenizer tokenizer) throws Exception
    {
        if (tokenizer.hasMoreElements())
        {
            String token = tokenizer.nextToken();
            try
            {
                return Integer.valueOf(token).intValue();
            }
            catch (NumberFormatException e)
            {
                throw new Exception("Nombre attendu a la place de \"" + token + "\"");
            }
        }
        else
            throw new Exception("Autre parametre attendu");
    }

    static java.sql.Date readDate(StringTokenizer tokenizer) throws Exception
    {
        if (tokenizer.hasMoreElements())
        {
            String token = tokenizer.nextToken();
            try
            {
                return java.sql.Date.valueOf(token);
            }
            catch (IllegalArgumentException e)
            {
                throw new Exception("Date dans un format invalide - \"" + token + "\"");
            }
        }
        else
            throw new Exception("Autre parametre attendu");
    }

}
