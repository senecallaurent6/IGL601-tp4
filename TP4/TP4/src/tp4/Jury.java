package tp4;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Jury
{
    @Id
    @GeneratedValue
    private long idBD;
    private int nas;
    private String prenom;
    private String nom;
    private String sexe;
    private int age;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    List<Proces> proces;

    public Jury(int nas, String prenom, String nom, String sexe, int age)
    {
        setNas(nas);
        setPrenom(prenom);
        setNom(nom);
        setSexe(sexe);
        setAge(age);

        proces = new LinkedList<Proces>();
    }

    public int getNas()
    {
        return nas;
    }

    public void setNas(int nas)
    {
        this.nas = nas;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public String getSexe()
    {
        return sexe;
    }

    public void setSexe(String sexe)
    {
        this.sexe = sexe;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public List<Proces> getProces()
    {
        return proces;
    }

    public void addProces(Proces p)
    {
        proces.add(p);
    }

    public boolean estDisponible()
    {
        for (Proces p : proces)
        {
            if (!p.estTermine())
            {
                return false;
            }
        }
        return true;
    }
}
