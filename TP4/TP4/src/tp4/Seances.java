package tp4;

import java.util.List;

import javax.persistence.TypedQuery;

public class Seances
{
    private TypedQuery<Seance> trouver;

    public Seances(Connexion cx)
    {
        trouver = cx.getConnection().createQuery("select tuple from Seance tuple where tuple.id=:id", Seance.class);
    }

    public Seance trouver(int id)
    {
        trouver.setParameter("id", id);
        List<Seance> tuples = trouver.getResultList();
        return tuples.isEmpty() ? null : tuples.get(0);
    }

    public boolean existe(int id)
    {
        return trouver(id) != null;
    }
}
